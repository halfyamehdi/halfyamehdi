export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: "vuejs",
    htmlAttrs: {
      lang: "fr",
    },
    meta: [
      {
        charset: "utf-8",
      },
      {
        name: "viewport",
        content: "width=device-width, initial-scale=1",
      },
      {
        hid: "description",
        name: "description",
        content: "",
      },
      {
        name: "format-detection",
        content: "telephone=no",
      },
    ],
    script: [
      {
        src: "/js/popper.min.js",
      },
      {
        src: "/js/jquery.min.js",
      },
      {
        src: "/js/bootstrap.min.js",
      },
    ],
    link: [
      {
        rel: "stylesheet",
        href: "/css/bootstrap.min.css",
      },
      {
        rel: "stylesheet",
        href: "./assets/index.less",
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ["@/assets/style.less"],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: ["@nuxtjs/proxy", "@nuxtjs/axios"],

  axios: {
    proxy: true,
  },

  modules: [, "@nuxtjs/axios"],
  proxy: {
    "/api/": {
      target: "http://localhost:5000/",
    },
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
};
