module.exports = (sequelize, DataTypes) => {
  return sequelize.define(
    "climatique",
    {
      id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
      },
      latitude: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      longitude: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      generationtime_ms: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      utc_offset_seconds: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      timezone: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      timezone_abbreviation: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      elevation: {
        type: DataTypes.STRING,
        allowNull: false,
      },
    },
    {
      timestamps: true,
      createdAt: "created",
      updatedAt: false,
    }
  );
};
