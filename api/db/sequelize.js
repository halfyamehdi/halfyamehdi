const { Sequelize, DataTypes } = require("sequelize");

const climatiqueModule = require("../models/climatique");

const bcrypt = require("bcrypt");
const data = require("../data/data");
const sequelize = new Sequelize("coche", "root", "", {
  host: "localhost",
  dialect: "mariadb",
  dialectOptions: {
    timezone: "Etc/GMT-2",
  },
  logging: false,
});
var climatique =climatiqueModule(sequelize, DataTypes);
const initDb = () => {
  return sequelize
    .sync({ force: true })
    .then((_) => {
      data.map((el) => {
        climatique.create(el);
      //});
    })
    .catch((e) => console.log("eror   : " + e));
};

module.exports = {
  initDb,
  clinte,
};
