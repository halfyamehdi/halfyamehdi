const { climatique } = require("../db/sequelize");
const { Op } = require("sequelize");
exports.add = (req, res) => {
  climatique
    .create(req.body)
    .then((p) => {
      const message = `climatique il a ajoute ok`;
      res.json({ message, data: p });
    })
    .catch((erro) => {
      const message = "erro 500.";
      res.status(500).json({ message, data: erro });
    });
};
exports.supprim = (req, res) => {
  climatique
    .findByPk(req.params.id)
    .then((p) => {
      if (p === null) {
        const message = "erro  404";
        return res.status(404).json({ message });
      }
      return climatique
        .destroy({
          where: { id: p.id },
        })
        .then((_) => {
          const message = `la climatique avec l'identifiant n°${p.id} a bien été supprimé.`;
          res.json({ message, data: p });
        });
    })
    .catch((erro) => {
      const message = "erro 500.";
      res.statu(500).json({ message, data: erro });
    });
};
exports.list = (req, res) => {
  
  climatique
      .findAll()
      .then((p) => {
        const message = "list de climatique.";

        res.json({ message, data: p });
      })
      .catch((erro) => {
        const message = "erro 500.";

        res.status(500).json({ message, data: erro });
      });
  
};

exports.update = (req, res) => {
  const id = req.params.id;
  climatique
    .update(req.body, {
      where: { id: id },
    })
    .then((_) => {
      return climatique.findByPk(id).then((p) => {
        if (p === null) {
          const message = "erro 404";
          return res.status(404).json({ message });
        }
        const message = `Le pokémon  a bien été modifié.`;
        res.json({ message, data: p });
      });
    })
    .catch((erro) => {
      const message = "erro 500.";
      res.statu(500).json({ message, data: erro });
    });
};
