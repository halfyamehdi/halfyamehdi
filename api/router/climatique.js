const { list, add, supprim, update } = require("../controller/climatique");

module.exports = (app) => {
  app.get("/api/climatique/", (req, res) => {
    list(req, res);
  });

  app.post("/api/climatique", (req, res) => {
    add(req, res);
  });
  app.delete("/api/climatique/:id", (req, res) => {
    supprim(req, res);
  });
  app.put("/api/climatique/:id", (req, res) => {
    update(req, res);
  });
};
